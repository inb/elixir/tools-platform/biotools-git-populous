/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.git.populous;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import es.bsc.inb.elixir.biotools.model.Tool;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyOrderStrategy;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.util.SystemReader;

/**
 * @author Dmitry Repchevsky
 */

public class Main {
    
    private final static String HELP = 
            "java -jar biotools-populouse.jar -g -u -p [-b] [--path] [--dirs]\n\n" +
            "parameters:\n\n" +
            "-h (--help)                - this help message\n" +
            "-g (--git)                 - git endpoint\n" +
            "-u (--user) 'username'     - github username\n" +
            "-p (--password) 'password' - github pasword\n" +
            "-b (--branch)              - git remote branch ['origin/master']\n" +
            "--path                     - git path to put files ['/']\n" +
            "--dirs                     - if present, put each tool into its own directory \n\n" +
            "example: >java -jar biotools-populous.jar -g https://github.com/xyz.git --path /biotools -u redmitry -p xyz\n";

    public final static String BRANCH = "origin/master";
    
    public static void main(String[] args) {
        
        Map<String, List<String>> params = parameters(args);
        
        if (params.get("-h") != null ||
            params.get("--help") != null) {
            System.out.println(HELP);
            System.exit(0);
        }

        List<String> git = params.get("-g");
        if (git == null) {
            git = params.get("--git");
        }

        final List<String> path = params.get("--path");
        final List<String> suffix = params.get("--suffix");
        final String sfx = suffix == null || suffix.isEmpty() ? ".oeb.json" : suffix.get(0);
        
        List<String> user = params.get("-u");
        if (user == null) {
            user = params.get("--user");
        }

        List<String> password = params.get("-p");
        if (password == null) {
            password = params.get("--password");
        }

        final String g = git == null || git.isEmpty() ? null : git.get(0);
        final String u = user == null || user.isEmpty() ? null : user.get(0);
        final String p = password == null || password.isEmpty() ? null : password.get(0);

        if (g == null || g.isEmpty() || u == null || u.isEmpty() || p == null || p.isEmpty()) {
            System.out.println(HELP);
        } else {
            List<String> branch = params.get("-b");
            if (branch == null) {
                branch = params.get("--branch");
            }
            final String b = branch == null || branch.isEmpty() ? null : branch.get(0);
            
            process(g, b != null ? b : BRANCH, 
                    path == null || path.isEmpty() ? "/" : path.get(0), 
                    params.get("--dirs") != null, sfx, u, p);
        }
    }
    
    private static void process(final String endpoint, final String branch, final String path,
            final boolean dirs, final String suffix, final String user, final String password) {
        
        final int idx = branch.lastIndexOf('/');
        final String local = idx < 0 ? branch : branch.substring(idx + 1);
        
        SystemReader old = SystemReader.getInstance();
        SystemReader.setInstance(new JgitSytemReaderHack(old));

        Configuration config = Configuration.unix().toBuilder()
                                    .setWorkingDirectory("/")
                                    .build();

        final FileSystem fs = Jimfs.newFileSystem(config);
        final Path root = fs.getPath("/");
        
        try (Git git = Git.cloneRepository()
                         .setURI(endpoint)
                         .setDirectory(root)
                         .setBranch(local)
                         .setRemote(branch)
                         .setCredentialsProvider(new UsernamePasswordCredentialsProvider(user, password))
                         .call()) {
            
            final Jsonb jsonb = JsonbBuilder.create(
                    new JsonbConfig().withPropertyOrderStrategy(PropertyOrderStrategy.LEXICOGRAPHICAL)
                                     .withFormatting(true));
            
            final BiotoolsRepositoryIterator iter = new BiotoolsRepositoryIterator();
            while(iter.hasNext()) {
                final JsonObject jtool = iter.next();

                final Tool tool = BiotoolsConverter.convert(jtool);
                
                final String _id = tool.getSummary().getToolID().trim().toLowerCase();
                
                final Path dir = dirs ? root.resolve(path).resolve(_id) : root.resolve(path);
                if (!Files.exists(dir)) {
                    try {
                        Files.createDirectories(dir);
                    } catch(IOException ex) {
                         Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "can't create dir: " + dir, ex.getMessage());
                    }
                }

                final Path file_path = dir.resolve(_id + suffix);

                try (Writer writer = Files.newBufferedWriter(file_path)) {
                    jsonb.toJson(tool, writer);
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            git.add().addFilepattern(".").call();
            git.commit().setMessage("biotools commit " + Instant.now()).call();
            git.push().setRemote(branch).setCredentialsProvider(
                    new UsernamePasswordCredentialsProvider(user, password)).call();
        } 
        catch (GitAPIException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(1);
        }


    }
    
    private static Map<String, List<String>> parameters(String[] args) {
        final TreeMap<String, List<String>> parameters = new TreeMap();        
        List<String> values = null;
        for (String arg : args) {
            switch(arg) {
                case "-g":
                case "--git":
                case "-b":
                case "--branch":
                case "--path":
                case "--dirs":
                case "--suffix":
                case "-u":
                case "--user":
                case "-p":
                case "--password":
                case "-h":
                case "--help": values = parameters.get(arg);
                               if (values == null) {
                                   values = new ArrayList(); 
                                   parameters.put(arg, values);
                               }
                               break;
                default: if (values != null) {
                    values.add(arg);
                }
            }
        }
        return parameters;
    }

}
