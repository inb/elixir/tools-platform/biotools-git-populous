/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.biotools.git.populous;

import es.bsc.inb.elixir.biotools.model.AccessibilityType;
import es.bsc.inb.elixir.biotools.model.CostType;
import es.bsc.inb.elixir.biotools.model.Credit;
import es.bsc.inb.elixir.biotools.model.DataType;
import es.bsc.inb.elixir.biotools.model.Documentation;
import es.bsc.inb.elixir.biotools.model.DocumentationType;
import es.bsc.inb.elixir.biotools.model.Download;
import es.bsc.inb.elixir.biotools.model.DownloadType;
import es.bsc.inb.elixir.biotools.model.EDAMdata;
import es.bsc.inb.elixir.biotools.model.EDAMformat;
import es.bsc.inb.elixir.biotools.model.ElixirNode;
import es.bsc.inb.elixir.biotools.model.ElixirPlatform;
import es.bsc.inb.elixir.biotools.model.EntityType;
import es.bsc.inb.elixir.biotools.model.ExternalId;
import es.bsc.inb.elixir.biotools.model.ExternalIdType;
import es.bsc.inb.elixir.biotools.model.Function;
import es.bsc.inb.elixir.biotools.model.Input;
import es.bsc.inb.elixir.biotools.model.Labels;
import es.bsc.inb.elixir.biotools.model.LanguageType;
import es.bsc.inb.elixir.biotools.model.LicenseType;
import es.bsc.inb.elixir.biotools.model.Link;
import es.bsc.inb.elixir.biotools.model.MaturityType;
import es.bsc.inb.elixir.biotools.model.OperatingSystemType;
import es.bsc.inb.elixir.biotools.model.Operation;
import es.bsc.inb.elixir.biotools.model.Output;
import es.bsc.inb.elixir.biotools.model.Publication;
import es.bsc.inb.elixir.biotools.model.PublicationType;
import es.bsc.inb.elixir.biotools.model.RoleType;
import es.bsc.inb.elixir.biotools.model.Summary;
import es.bsc.inb.elixir.biotools.model.Tool;
import es.bsc.inb.elixir.biotools.model.ToolLinkType;
import es.bsc.inb.elixir.biotools.model.ToolType;
import es.bsc.inb.elixir.biotools.model.Topic;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

/**
 * @author Dmitry Repchevsky
 */

public class BiotoolsConverter {
   
    public static Tool convert(JsonObject jtool) {
        
        final String id = jtool.getString("biotoolsID", null);
        if (id == null || id.isEmpty()) {
            return null;
        }

        final Tool tool = new Tool();
        
        final Summary summary = new Summary();
        tool.setSummary(summary);
        
        summary.setToolID(id);
        summary.setName(jtool.getString("name", null));
        summary.setHomepage(jtool.getString("homepage", null));
        summary.setDescription(jtool.getString("description", null));
        summary.setCurieID(jtool.getString("biotoolsCURIE", null));
        
        final JsonArray jversions = jtool.getJsonArray("version");
        if (jversions != null && jversions.size() > 0) {
            final List<String> versions = new ArrayList<>(jversions.size());
            summary.setVersions(versions);
            for (int i = 0, n = jversions.size(); i < n; i++) {
                versions.add(jversions.getString(i));
            }
        }

        addOtherIDs(summary, jtool);
        addDocumentation(tool, jtool);
        addPublications(tool, jtool);
        addCredits(tool, jtool);
        addFunctions(tool, jtool);
        addDownloads(tool, jtool);
        addLinks(tool, jtool);
        addTopics(tool, jtool);

        // Labels
        setCollectionIDs(tool, jtool);
        setLanguages(tool, jtool);
        setLicense(tool, jtool);
        setMaturity(tool, jtool);
        setCost(tool, jtool);
        setToolTypes(tool, jtool);
        setOperatingSystems(tool, jtool);
        setAccessibilities(tool, jtool);
        setElixirPlatforms(tool, jtool);
        setElixirNodes(tool, jtool);

        return tool;
    }
    
    private static void setCollectionIDs(Tool tool, JsonObject jtool) {
        final JsonArray jcollectionIDs = jtool.getJsonArray("collectionID");
        if (jcollectionIDs != null && jcollectionIDs.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null) {
                tool.setLabels(labels = new Labels());
            }
            final List<String> collection_ids = new ArrayList<>(jcollectionIDs.size());
            labels.setCollectionIDs(collection_ids);
            for (int i = 0, n = jcollectionIDs.size(); i < n; i++) {
                final String collection_id = jcollectionIDs.getString(i, null);
                if (collection_id != null) {
                    collection_ids.add(collection_id);
                }
            }
        }
    }
    
    private static void setToolTypes(Tool tool, JsonObject jtool) {
        final JsonArray jtoolTypes = jtool.getJsonArray("toolType");
        if (jtoolTypes != null && jtoolTypes.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null) {
                tool.setLabels(labels = new Labels());
            }
            final List<ToolType> tool_types = new ArrayList<>(jtoolTypes.size());
            labels.setToolTypes(tool_types);
            for (int i = 0, n = jtoolTypes.size(); i < n; i++) {
                final String tool_type = jtoolTypes.getString(i, null);
                if (tool_type != null) {
                    try {
                        tool_types.add(ToolType.fromValue(tool_type));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized tool type: {0}", tool_type);
                    }
                }
            }
        }
    }

    private static void setLicense(Tool tool, JsonObject jtool) {
        final String license = jtool.getString("license", null);
        if (license != null) {
            Labels labels = tool.getLabels();
            if (labels == null) {
                tool.setLabels(labels = new Labels());
            }
            try {
                labels.setLicense(LicenseType.fromValue(license));
            } catch(IllegalArgumentException ex) {
                Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized license: {0}", license);
            }
        }
    }
    
    private static void setMaturity(Tool tool, JsonObject jtool) {
        final String maturity = jtool.getString("maturity", null);
        if (maturity != null) {
            Labels labels = tool.getLabels();
            if (labels == null){
                tool.setLabels(labels = new Labels());
            }
            try {
                labels.setMaturity(MaturityType.fromValue(maturity));
            } catch(IllegalArgumentException ex) {
                Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized maturity: {0}", maturity);
            }
        }
    }

    private static void setCost(Tool tool, JsonObject jtool) {
        final String cost = jtool.getString("cost", null);
        if (cost != null) {
            Labels labels = tool.getLabels();
            if (labels == null){
                tool.setLabels(labels = new Labels());
            }
            try {
                labels.setCost(CostType.fromValue(cost));
            } catch(IllegalArgumentException ex) {
                Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized cost: {0}", cost);
            }
        }
    }
    
    private static void setLanguages(Tool tool, JsonObject jtool) {
        final JsonArray jlanguages = jtool.getJsonArray("language");
        if (jlanguages != null && jlanguages.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null) {
                tool.setLabels(labels = new Labels());
            }
            final List<LanguageType> languages = new ArrayList<>(jlanguages.size());
            labels.setLanguages(languages);
            for (int i = 0, n = jlanguages.size(); i < n; i++) {
               final String language = jlanguages.getString(i, null);
               if (language != null) {
                    try {
                        languages.add(LanguageType.fromValue(language));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized language : {0}", language);
                    }
               }
            }
        }
    }

    private static void setOperatingSystems(Tool tool, JsonObject jtool) {
        final JsonArray jOperatingSystems = jtool.getJsonArray("operatingSystem");
        if (jOperatingSystems != null && jOperatingSystems.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null){
                tool.setLabels(labels = new Labels());
            }
            
            final List<OperatingSystemType> operationg_systems = new ArrayList<>(jOperatingSystems.size());
            labels.setOperatingSystems(operationg_systems);
            for (int i = 0, n = jOperatingSystems.size(); i < n; i++) {
                final String operatingSystem = jOperatingSystems.getString(i, null);
                if (operatingSystem != null) {
                    try {
                        operationg_systems.add(OperatingSystemType.fromValue(operatingSystem));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized operating system : {0}", operatingSystem);
                    }
                }
            }
        }
    }
    
    private static void setAccessibilities(Tool tool, JsonObject jtool) {
        final JsonArray jAccessibilities = jtool.getJsonArray("accessibility");
        if (jAccessibilities != null && jAccessibilities.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null){
                tool.setLabels(labels = new Labels());
            }
            
            final List<AccessibilityType> accessibilities = new ArrayList<>(jAccessibilities.size());
            labels.setAccessibilities(accessibilities);
            for (int i = 0, n = jAccessibilities.size(); i < n; i++) {
                final String accessibility = jAccessibilities.getString(i, null);
                if (accessibility != null) {
                    try {
                        accessibilities.add(AccessibilityType.fromValue(accessibility));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized accessibility : {0}", accessibility);
                    }
                }
            }
        }
    }
    
    private static void setElixirPlatforms(Tool tool, JsonObject jtool) {
        final JsonArray jElixirPlatforms = jtool.getJsonArray("elixirPlatform");
        if (jElixirPlatforms != null && jElixirPlatforms.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null){
                tool.setLabels(labels = new Labels());
            }
            final List<ElixirPlatform> elixir_platforms = new ArrayList<>(jElixirPlatforms.size());
            labels.setElixirPlatforms(elixir_platforms);
            for (int i = 0, n = jElixirPlatforms.size(); i < n; i++) {
                final String elixir_platform = jElixirPlatforms.getString(i, null);
                if (elixir_platform != null) {
                    try {
                        elixir_platforms.add(ElixirPlatform.fromValue(elixir_platform));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized ELIXIR platform : {0}", elixir_platform);
                    }
                }
            }
        }
    }
    
    private static void setElixirNodes(Tool tool, JsonObject jtool) {
        final JsonArray jElixirNode = jtool.getJsonArray("elixirNode");
        if (jElixirNode != null && jElixirNode.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null){
                tool.setLabels(labels = new Labels());
            }
            final List<ElixirNode> elixir_nodes = new ArrayList<>(jElixirNode.size());
            labels.setElixirNodes(elixir_nodes);
            for (int i = 0, n = jElixirNode.size(); i < n; i++) {
                final String elixir_node = jElixirNode.getString(i, null);
                if (elixir_node != null) {
                    try {
                        elixir_nodes.add(ElixirNode.fromValue(elixir_node));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized ELIXIR node : {0}", elixir_node);
                    }
                }
            }
        }
    }

    private static void addOtherIDs(Summary summary, JsonObject jtool) {
        final JsonArray jOtherIDs = jtool.getJsonArray("otherID");
        if (jOtherIDs != null && jOtherIDs.size() > 0) {
            final List<ExternalId> external_ids = new ArrayList<>(jOtherIDs.size());
            summary.setOtherIDs(external_ids);
            for (int i = 0, n = jOtherIDs.size(); i < n; i++) {
                final JsonObject jOtherID = jOtherIDs.getJsonObject(i);
                final ExternalId external_id = new ExternalId();
                external_ids.add(external_id);
                
                external_id.setValue(jOtherID.getString("value", null));
                external_id.setVersion(jOtherID.getString("version", null));
                
                final String type = jOtherID.getString("type", null);
                if (type != null) {
                    try {
                        external_id.setType(ExternalIdType.fromValue(type));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized 'other id' type : {0}", type);
                    }
                }
            }
        }

    }
    
    private static void addDocumentation(Tool tool, JsonObject jtool) {
        final JsonArray jdocumentations = jtool.getJsonArray("documentation");
        if (jdocumentations != null && jdocumentations.size() > 0) {
            final List<Documentation> documentations = new ArrayList<>(jdocumentations.size());
            tool.setDocumentations(documentations);
            for (int i = 0, n = jdocumentations.size(); i < n; i++) {
                final JsonObject jdocumentation = jdocumentations.getJsonObject(i);
                final Documentation documentation = new Documentation();
                documentations.add(documentation);
                documentation.setUrl(jdocumentation.getString("url", null));
                final String type = jdocumentation.getString("type", null);
                if (type != null) {
                    try {
                        documentation.setType(DocumentationType.fromValue(type));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized documentation type: {0}", type);
                    }
                }
            }
        }
    }
    
    private static void addPublications(Tool tool, JsonObject jtool) {
        final JsonArray jpublications = jtool.getJsonArray("publication");
        if (jpublications != null && jpublications.size() > 0) { 
            final List<Publication> publications = new ArrayList<>(jpublications.size());
            tool.setPublications(publications);
            for (int i = 0, n = jpublications.size(); i < n; i++) {
                final JsonObject jpublication = jpublications.getJsonObject(i);
                final Publication publication = new Publication();
                publications.add(publication);
                
                publication.setDoi(jpublication.getString("doi", null));
                publication.setPmid(jpublication.getString("pmid", null));
                publication.setPmcid(jpublication.getString("pmcid", null));
                publication.setVersion(jpublication.getString("version", null));

                final String type = jpublication.getString("type", null);
                if (type != null) {
                    try {
                        publication.setType(PublicationType.fromValue(type));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized publication type: {0}", type);
                    }
                }
                
                final JsonValue jmetadata_value = jpublication.get("metadata");
                if (jmetadata_value != null && jmetadata_value.getValueType() == ValueType.OBJECT) {
                    final JsonObject jmetadata = jmetadata_value.asJsonObject();

                    final String title = jmetadata.getString("title", null);
                    final String abridge = jmetadata.getString("abstract", null);
                    final String journal = jmetadata.getString("journal", null);
                    
                    publication.setTitle(title);
                    publication.setAbstract(abridge);
                    publication.setJournal(journal);
                    
                    final String date = jmetadata.getString("date", null);
                    if (date != null) {
                        try {
                            final ZonedDateTime date_time = ZonedDateTime.parse(date);
                            publication.setYear(date_time.toLocalDate().toString());
                        } catch (DateTimeParseException ex) {
                            Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "invalid date format: {0}", date);
                        }
                    }
                    
                    final JsonNumber cit_count = jmetadata.getJsonNumber("citationCount");
                    if (cit_count != null) {
                        publication.setCitationsCount(cit_count.intValue());
                    }
                    
                    final JsonArray jauthors = jmetadata.getJsonArray("authors");
                    if (jauthors != null && jauthors.size() > 0) {
                        final List<String> authors = new ArrayList(jauthors.size());
                        publication.setAuthors(authors);
                        for (int j = 0, m = jauthors.size(); j < m; j++) {
                            final JsonObject jauthor = jauthors.getJsonObject(j);
                            if (jauthor != null) {
                                final String author = jauthor.getString("name", null);
                                if (author != null) {
                                    authors.add(author);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private static void addCredits(Tool tool, JsonObject jtool) {
        final JsonArray jcredits = jtool.getJsonArray("credit");
        if (jcredits != null) {
            final List<Credit> credits = new ArrayList<>(jcredits.size());
            tool.setCredits(credits);
            for (int i = 0, n = jcredits.size(); i < n; i++) {
                JsonObject jcredit = jcredits.getJsonObject(i);
                final Credit credit = new Credit();
                credits.add(credit);
                
                credit.setName(jcredit.getString("name", null));
                credit.setEmail(jcredit.getString("email", null));
                credit.setOrcidId(jcredit.getString("orcidId", null));
                credit.setNote(jcredit.getString("note", null));
                credit.setUrl(jcredit.getString("url", null));

                final String type = jcredit.getString("typeEntity", null);
                if (type != null) {
                    try {
                        credit.setTypeEntity(EntityType.fromValue(type));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized entity type: {0}", type);
                    }
                }

                final JsonArray jtypeRoles = jcredit.getJsonArray("typeRole");
                if (jtypeRoles != null) {
                    final List<RoleType> typeRoles = new ArrayList<>(jtypeRoles.size());
                    credit.setTypeRoles(typeRoles);
                    for (int j = 0, m = jtypeRoles.size(); j < n; j++) {
                        final String role = jtypeRoles.getString(j, null);
                        if (role != null) {
                            try {
                                typeRoles.add(RoleType.fromValue(role));
                            } catch(IllegalArgumentException ex) {
                                Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized role role: {0}", type);
                            }
                        }
                    }
                }
            }
        }
    }

    private static void addFunctions(Tool tool, JsonObject jtool) {
        final JsonArray jfunctions = jtool.getJsonArray("function");
        if (jfunctions != null && jfunctions.size() > 0) {
            final List<Function> functions = new ArrayList<>(jfunctions.size());
            tool.setFunctions(functions);
            for (int i = 0, n = jfunctions.size(); i < n; i++) {
                final JsonObject jfunction = jfunctions.getJsonObject(i);
                final Function function = new Function();
                functions.add(function);
                addOperations(function, jfunction);

                final JsonArray jinputs = jfunction.getJsonArray("input");
                if (jinputs != null && jinputs.size() > 0) {
                    final List<Input> inputs = new ArrayList<>(jinputs.size());
                    function.setInputs(inputs);
                    for (int j = 0, m = jinputs.size(); j < m; j++) {
                        final Input input = new Input();
                        inputs.add(input);
                        addDatatype(input, jinputs.getJsonObject(j));
                    }
                }

                final JsonArray joutputs = jfunction.getJsonArray("output");
                if (joutputs != null && joutputs.size() > 0) {
                    final List<Output> outputs = new ArrayList<>(jinputs.size());
                    function.setOutputs(outputs);
                    for (int j = 0, m = joutputs.size(); j < m; j++) {
                        final Output output = new Output();
                        outputs.add(output);
                        addDatatype(output, joutputs.getJsonObject(j));
                    }
                }
                
                function.setCmd(jfunction.getString("cmd", null));
                function.setNote(jfunction.getString("note", null));
            }
        }
    }
    
    private static void addOperations(Function function, JsonObject jfunction) {
        final JsonArray joperations = jfunction.getJsonArray("operation");
        if (joperations != null && joperations.size() > 0) {
            final List<Operation> operations = new ArrayList<>(joperations.size());
            function.setOperations(operations);
            for (int i = 0, n = joperations.size(); i < n; i++) {
                final JsonObject joperation = joperations.getJsonObject(i);
                final Operation operation = new Operation();
                operations.add(operation);
                
                operation.setUri(joperation.getString("uri", null));
                operation.setTerm(joperation.getString("term", null));
            }
        }
    }

    private static void addDatatype(DataType datatype, JsonObject jdatatype) {
        
        final JsonObject jdata = jdatatype.getJsonObject("data");
        if (jdata != null) {
            final EDAMdata data = new EDAMdata();
            datatype.setData(data);
            data.setUri(jdata.getString("uri", null));
            data.setTerm(jdata.getString("term", null));
        }
            
        final JsonArray jformats = jdatatype.getJsonArray("format");
        if (jformats != null && jformats.size() > 0) {
            final List<EDAMformat> formats = new ArrayList<>();
            datatype.setFormats(formats);
            for (int i = 0, n = jformats.size(); i < n; i++) {
                final JsonObject jformat = jformats.getJsonObject(i);
                final EDAMformat format = new EDAMformat();
                formats.add(format);
                format.setUri(jformat.getString("uri", null));
                format.setTerm(jformat.getString("term", null));
            }
        }
    }

    private static void addDownloads(Tool tool, JsonObject jtool) {
        final JsonArray jdownloads = jtool.getJsonArray("download");
        if (jdownloads != null && jdownloads.size() > 0) {
            final List<Download> downloads = new ArrayList<>(jdownloads.size());
            tool.setDownloads(downloads);
            for (int i = 0, n = jdownloads.size(); i < n; i++) {
                final JsonObject jdownload = jdownloads.getJsonObject(i);
                final Download download = new Download();
                downloads.add(download);
                
                download.setUrlFtpType(jdownload.getString("url", null));
                download.setNote(jdownload.getString("note", null));
                download.setVersion(jdownload.getString("version", null));

                final String type = jdownload.getString("type", null);
                if (type != null) {
                    try {
                        download.setDownloadType(DownloadType.fromValue(type));
                    } catch(IllegalArgumentException ex) {
                        Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized download type: {0}", type);
                    }
                }
            }
        }
    }

    private static void addLinks(Tool tool, JsonObject jtool) {
        final JsonArray jlinks = jtool.getJsonArray("link");
        if (jlinks != null && jlinks.size() > 0) {
            final List<Link> links = new ArrayList<>(jlinks.size());
            tool.setLinks(links);
            for (int i = 0, n = jlinks.size(); i < n; i++) {
                final JsonObject jlink = jlinks.getJsonObject(i);
                final Link link = new Link();
                links.add(link);
                
                link.setUrl(jlink.getString("url", null));
                link.senNote(jlink.getString("note", null));

                final String type = jlink.getString("type", null);
                if (type != null) {
                    try {
                        link.setType(ToolLinkType.fromValue(type));
                    } catch(IllegalArgumentException ex) {
                       Logger.getLogger(BiotoolsConverter.class.getName()).log(Level.INFO, "unrecognized link type: {0}", type);
                    }
                }
            }
        }
    }

    private static void addTopics(Tool tool, JsonObject jtool) {
        final JsonArray jtopics = jtool.getJsonArray("topic");
        if (jtopics != null && jtopics.size() > 0) {
            Labels labels = tool.getLabels();
            if (labels == null){
                tool.setLabels(labels = new Labels());
            }
            final List<Topic> topics = new ArrayList<>(jtopics.size());
            labels.setTopics(topics);
            for (int i = 0, n = jtopics.size(); i < n; i++) {
                final JsonObject jtopic = jtopics.getJsonObject(i);
                final Topic topic = new Topic();
                topics.add(topic);
                
                topic.setUri(jtopic.getString("uri", null));
                topic.setTerm(jtopic.getString("term", null));
            }
        }
    }
}
