### Biotools Populous is a utility to push biotools data into a git repository.

The utility is based on modified version of [JGit](https://github.com/redmitry/jgit) libarary based on [NIO2](https://jcp.org/en/jsr/detail?id=203) API and 
[Jimfs](https://github.com/google/jimfs) in-memory file system. It allows wirking with git repositories directly, without writing anything on a disk.

To get tools data, utility uses the Biotools REST API (https://bio.tools/api/tool/).
Generated files are in JSON format inferred from [biotoolsSchema 3.1](https://github.com/bio-tools/biotoolsSchema).

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/elixir/tools-platform/biotools-git-populous.git
>cd biotools-git-populous
>mvn install
```

It will create **biotools-populous.jar** file in the **biotools-git-populous/target/** directory.

Running:
```shell
>java -jar biotools-populous.jar -g https://gitlab.bsc.es/xyz -u user -p password --path /biotools
```

where:

* -h         - help
* --git      - git repository to push tools json files
* --branch   - remote branch to push
* --path     - remote path to put files
* --user     - git user name
* --password - git user password
